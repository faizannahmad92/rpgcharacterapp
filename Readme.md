# RPGCharacterApp

RPGCharacterApp is a console application in Java. In this application one can create characters of different class. The characters have different stats based on their classes 
and their stats improve when they gain levels. The three base attributes are strength, dexterity and intelligence.

The characters can also equip weapon and armour. Equipment of different armors
can enhance their attributes. Equipping different kinds of weapons increases their attack power.

## Technologies

Java 17

## Types

Basic attributes:
- Strength
- Dexterity
- Intelligence

Equipment:
- Weapons
  - allowed equipment slots: weapon
- Armour
  - allowed equipment slots: head, body, legs

The character types included:

- Mage
  - Primary atrribute: Intelligence
  - Allowed weapons: Staff, Wand
  - Allowed armour: Cloth
- Warrior
  - Primary atrribute: Dexterity
  - Allowed weapons: Bow
  - Allowed armour: Leather, Mail
- Rogue
  - Primary atrribute: Dexterity
  - Allowed weapons: Dagger, Sword
  - Allowed armour: Leather, Mail
- Ranger
  - Primary atrribute: Strength
  - Allowed weapons: Axe, Hammer, Sword
  - Allowed armour: Mail, Plate


## Usage

Code Example:
```java
        
        //create a character (name)
        Mage mage = new Mage("Raistlin Majere");
        
        //level up a character
        mage.levelUp();
        
        //create a weapon (name, requiredLevel, type, damage, attackspeed)
        Weapon staffOfMagius = new Weapon ("Staff of Magius", 2, WeaponType.STAFF, 4, 1);
        

        //create an armor (name, requiredLevel, type, damage, attackspeed, bonusAttributes(strength, dexterity, intelligence)))
        Armor mageRobe = new Armor("Standard Mage Robe", 1, ArmorType.CLOTH, new PrimaryAttributes(0,1,3)) ;

       
        //equip armor and weapons
        try{
            mage.equipItem(staffOfMagius, EquipmentSlot.WEAPON);
            mage.equipItem(mageRobe, EquipmentSlot.BODY);
        }catch(Exception e){
            System.out.println(e);
        }
        
        //show character sheet
        System.out.println(mage.showCharacterStats());
```

To then run from console:
```
cd ../src
javac Main.java
java Main.java
```
## Sources
This was a practise project made in the Experis Academy Full Stack Java Developer course .
