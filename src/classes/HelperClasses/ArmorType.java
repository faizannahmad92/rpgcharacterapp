package classes.HelperClasses;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
