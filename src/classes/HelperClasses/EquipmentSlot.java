package classes.HelperClasses;

public enum EquipmentSlot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
