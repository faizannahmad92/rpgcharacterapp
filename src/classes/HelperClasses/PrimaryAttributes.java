package classes.HelperClasses;

public class PrimaryAttributes {
    private int str;
    private int dex;
    private int intel;

    public PrimaryAttributes(int str, int dex, int intel) {
        this.str = str;
        this.dex = dex;
        this.intel= intel;
    }

    public int getStr() {
        return str;
    }

    public int getDex() {
        return dex;
    }

    public int getIntel() {
        return intel;
    }

    public void setAttributes(int str, int dex, int intel) {
        this.str = str;
        this.dex = dex;
        this.intel= intel;
    }

    @Override
    public String toString() {
        return "Strength: " + this.getStr() + ", Dexterity: " + this.getDex() + ", Intelligence: " + this.getIntel();
    }
}
