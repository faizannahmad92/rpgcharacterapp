package classes.HelperClasses;

public enum AttributeType
{
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE
}
