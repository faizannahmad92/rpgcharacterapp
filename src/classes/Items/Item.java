package classes.Items;
import classes.HelperClasses.EquipmentSlot;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private EquipmentSlot slot;

    public Item(String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot=null;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setSlot(EquipmentSlot slot) {
        this.slot = slot;
    }
}
