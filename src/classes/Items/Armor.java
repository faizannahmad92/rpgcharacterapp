package classes.Items;

import classes.HelperClasses.ArmorType;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;

public class Armor extends Item{
    private ArmorType type;
    private PrimaryAttributes bonusAttributes;

    public Armor(String name, int requiredLevel, ArmorType type, PrimaryAttributes bonusAttributes) {
        super(name, requiredLevel);
        this.type = type;
        this.bonusAttributes = bonusAttributes;
    }

    public PrimaryAttributes getBonusAttributes() {
        return bonusAttributes;
    }

    public ArmorType getType() {
        return type;
    }
}
