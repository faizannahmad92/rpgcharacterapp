package classes.Items;

import classes.HelperClasses.WeaponType;

public class Weapon extends Item {
    private WeaponType type;
    private int damage;
    private float attackSpeed;

    public Weapon(String name, int requiredLevel, WeaponType type, int damage, float attackSpeed) {
        super(name, requiredLevel);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public float getDPS(){
        return this.damage*this.attackSpeed;
    }
}
