package classes.CharacterClasses;

import classes.HelperClasses.ArmorType;
import classes.HelperClasses.AttributeType;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;

public class Roque extends Character {

    public Roque(String name){
        super(name);
        super.setBaseAttributes(new PrimaryAttributes(2,6,1));
        super.setTotalAttributes(super.getBaseAttributes());
        super.setAttrUsedForDamage(AttributeType.DEXTERITY);
        super.addAllowedWeaponsType(WeaponType.DAGGER);
        super.addAllowedWeaponsType(WeaponType.SWORD);
        super.addAllowedArmorType(ArmorType.MAIL);
        super.addAllowedArmorType(ArmorType.LEATHER);
    }

    @Override
    public void levelUp() {
        PrimaryAttributes oldAttr = super.getBaseAttributes(); //get the characters old attributes
        super.setBaseAttributes(new PrimaryAttributes(oldAttr.getStr() + 1, oldAttr.getDex() + 4,
                oldAttr.getIntel() + 1));  //increase old attributes
        super.setTotalAttributes(super.getBaseAttributes());
        super.setLevel(super.getLevel()+1);
    }
}
