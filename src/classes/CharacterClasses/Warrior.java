package classes.CharacterClasses;

import classes.HelperClasses.ArmorType;
import classes.HelperClasses.AttributeType;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;

public class Warrior extends Character {

    public Warrior(String name){
        super(name);
        super.setBaseAttributes(new PrimaryAttributes(5,2,1));
        super.setTotalAttributes(super.getBaseAttributes());
        super.setAttrUsedForDamage(AttributeType.STRENGTH);
        super.addAllowedWeaponsType(WeaponType.AXE);
        super.addAllowedWeaponsType(WeaponType.HAMMER);
        super.addAllowedWeaponsType(WeaponType.SWORD);
        super.addAllowedArmorType(ArmorType.MAIL);
        super.addAllowedArmorType(ArmorType.PLATE);
    }

    @Override
    public void levelUp() {
        PrimaryAttributes oldAttr = super.getBaseAttributes(); //get the characters old attributes
        super.setBaseAttributes(new PrimaryAttributes(oldAttr.getStr() + 3, oldAttr.getDex() + 2,
                oldAttr.getIntel() + 1));  //increase old attributes
        super.setTotalAttributes(super.getBaseAttributes());
        super.setLevel(super.getLevel()+1);
    }
}
