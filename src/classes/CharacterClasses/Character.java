
package classes.CharacterClasses;

import java.util.ArrayList;
import java.util.EnumMap;

import classes.Exceptions.InvalidWeaponException;
import classes.Exceptions.InvalidArmorException;
import classes.HelperClasses.*;
import classes.Items.Armor;
import classes.Items.Item;
import classes.Items.Weapon;

public abstract class Character {
    private String name;
    private int level;
    private PrimaryAttributes baseAttributes;
    private PrimaryAttributes totalAttributes;
    private float damage;
    private AttributeType attrUsedForDamage;
    private EnumMap<EquipmentSlot, Item> equipment;
    private ArrayList<WeaponType> allowedWeapons;
    private ArrayList<ArmorType> allowedArmors;



    public Character(String name) {
        this.name=name;
        this.level = 1;
        this.baseAttributes = new PrimaryAttributes(0,0,0);
        this.totalAttributes = new PrimaryAttributes(0,0,0);
        this.damage = 0;
        this.equipment = new EnumMap<EquipmentSlot, Item>(EquipmentSlot.class);
        this.allowedWeapons = new ArrayList<WeaponType>();
        this.allowedArmors = new ArrayList<ArmorType>();
        this.attrUsedForDamage = null;
    }

    public void setBaseAttributes(PrimaryAttributes baseAttributes) {

        this.baseAttributes = baseAttributes;
    }

    public void setTotalAttributes(PrimaryAttributes totalAttributes) {

        this.totalAttributes = totalAttributes;
    }


    public void setAttrUsedForDamage(AttributeType attrUsedForDamage) {
        this.attrUsedForDamage = attrUsedForDamage;
    }

    public PrimaryAttributes getBaseAttributes() {

        return baseAttributes;
    }

    public PrimaryAttributes getTotalAttributes() {

        return totalAttributes;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList getAllowedWeapons() {

        return allowedWeapons;
    }

    public void addAllowedWeaponsType(WeaponType type) {

        this.allowedWeapons.add(type);
    }

    public void addAllowedArmorType(ArmorType type) {

        this.allowedArmors.add(type);
    }


    public Item getSpecificEquipment(EquipmentSlot slot) {
        return equipment.get(slot);
    }

    //calculate total damage from attribute the given class uses for damage calculations and from weapon equiped.
    public void calculateDamage() {
        float attribute = 0;
        Weapon currentWeapon = (Weapon) this.equipment.get(EquipmentSlot.WEAPON);


        //determine the attribute used
        if(attrUsedForDamage == AttributeType.DEXTERITY){
            attribute = totalAttributes.getDex();
        }else if(attrUsedForDamage == AttributeType.STRENGTH){
            attribute = totalAttributes.getStr();
        }else if(attrUsedForDamage == AttributeType.INTELLIGENCE){
            attribute = totalAttributes.getIntel();
        }

        if (currentWeapon != null) {   //checks if weapon equipped
            //Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
            this.damage = (currentWeapon.getDPS() * (1 + attribute/ 100));
        } else {
            this.damage = (1 * (1 + attribute/ 100));
        }
    }

    public float getDamage() {
        return damage;
    }

    //Function to calculate totalAttributes. This adds bonuses from items to baseAttributes, and saves the stats as
    //characters totalAttributes
    public void calculateTotalAttributes(){

        //create new Attributes object with base attributes
        PrimaryAttributes baseAndBonuses = new PrimaryAttributes(this.baseAttributes.getStr(), this.baseAttributes.getDex(),
                this.baseAttributes.getIntel());   //get the base attributes for character

        //all characters armor equipmentslots to a list, weaponslot excluded
        ArrayList<EquipmentSlot> armorEquipmentslots  = new ArrayList<EquipmentSlot>();
        armorEquipmentslots.add(EquipmentSlot.HEAD);
        armorEquipmentslots.add(EquipmentSlot.BODY);
        armorEquipmentslots.add(EquipmentSlot.LEGS);

        //iterate over equipment slots and add all the bonuses from items to base attributes.
        for(EquipmentSlot slot : armorEquipmentslots){
            if(this.equipment.get(slot) != null){
                Armor equippedArmor = (Armor) this.equipment.get(slot);
                PrimaryAttributes bonusAttributes = equippedArmor.getBonusAttributes();

                baseAndBonuses.setAttributes((baseAndBonuses.getStr())+ bonusAttributes.getStr(),
                        (baseAndBonuses.getDex())+ bonusAttributes.getDex(),
                        (baseAndBonuses.getIntel())+ bonusAttributes.getIntel() );
            }
        }

        this.totalAttributes = baseAndBonuses;

        //calculate new damagevalue based on new attributes
        this.calculateDamage();

    }

    //Function to equip weapon and armor. Checks if item that is tried to equip meets character type and level requirements
    //if equip succesfull calls functions to calulate new dps and attributes for character.
    public void equipItem(Item item, EquipmentSlot slot) throws  InvalidWeaponException, InvalidArmorException{

        if (item instanceof Weapon) {
            Weapon newWeapon = (Weapon) item;
            if(allowedWeapons.contains(newWeapon.getType()) && slot== EquipmentSlot.WEAPON
            && this.level >= newWeapon.getRequiredLevel()){
                newWeapon.setSlot(EquipmentSlot.WEAPON);
                this.equipment.put(EquipmentSlot.WEAPON, newWeapon);
                this.calculateDamage();
            }else{
                throw new InvalidWeaponException("The weapon you are trying to equip is not allowed for this character type" +
                        "or the character does not have the required level to equip this item.");
            }

        }
        if (item instanceof Armor) {
            Armor newArmor = (Armor) item;

            if(allowedArmors.contains(newArmor.getType()) && this.level >= newArmor.getRequiredLevel()){
                newArmor.setSlot(slot);
                this.equipment.put(slot, newArmor);
                this.calculateTotalAttributes();
            }else{
                throw new InvalidArmorException("The weapon you are trying to equip is not allowed for this character type" +
                        "or the character does not have the required level to equip this item.");
            }
        }
    }

    //function to make character sheet
    public String showCharacterStats(){
        StringBuilder characterSheetBuilder = new StringBuilder();
        characterSheetBuilder.append("Character Stats: \n" );
        characterSheetBuilder.append("Name: " + this.name + "\n");
        characterSheetBuilder.append("Level: " + this.level + "\n");
        characterSheetBuilder.append("Strength: " + this.totalAttributes.getStr() + "\n");
        characterSheetBuilder.append("Dexterity: " + this.totalAttributes.getDex() + "\n");
        characterSheetBuilder.append("Intelligence: " + this.totalAttributes.getIntel() + "\n");
        characterSheetBuilder.append("DPS: " + this.damage + "\n");

        return characterSheetBuilder.toString();
    }

    public abstract void levelUp();


}
