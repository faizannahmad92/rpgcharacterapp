package classes.CharacterClasses;

import classes.HelperClasses.ArmorType;
import classes.HelperClasses.AttributeType;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;

public class Mage extends Character {

    public Mage(String name){
        super(name);
        super.setBaseAttributes(new PrimaryAttributes(1,1,8));
        super.setTotalAttributes(super.getBaseAttributes());
        super.setAttrUsedForDamage(AttributeType.INTELLIGENCE);
        super.addAllowedWeaponsType(WeaponType.STAFF);
        super.addAllowedWeaponsType(WeaponType.WAND);
        super.addAllowedArmorType(ArmorType.CLOTH);
    }

    @Override
    public void levelUp() {
        PrimaryAttributes oldAttr = super.getBaseAttributes(); //get the characters old attributes
        super.setBaseAttributes(new PrimaryAttributes(oldAttr.getStr() + 1, oldAttr.getDex() + 1,
                oldAttr.getIntel()+5));  //increase old attributes
        super.setTotalAttributes(super.getBaseAttributes());
        super.setLevel(super.getLevel()+1);

    }
}
