package classes.CharacterClasses;

import classes.HelperClasses.ArmorType;
import classes.HelperClasses.AttributeType;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;

public class Ranger extends Character {

    public Ranger(String name) {
        super(name);
        super.setBaseAttributes(new PrimaryAttributes(1, 7, 1));
        super.setTotalAttributes(super.getBaseAttributes());
        super.setAttrUsedForDamage(AttributeType.DEXTERITY);
        super.addAllowedWeaponsType(WeaponType.BOW);
        super.addAllowedArmorType(ArmorType.MAIL);
        super.addAllowedArmorType(ArmorType.LEATHER);
    }

    @Override
    public void levelUp() {
        PrimaryAttributes oldAttr = super.getBaseAttributes(); //get the characters old attributes
        super.setBaseAttributes(new PrimaryAttributes(oldAttr.getStr() + 1, oldAttr.getDex() + 5,
                oldAttr.getIntel() + 1));  //increase old attributes
        super.setTotalAttributes(super.getBaseAttributes());
        super.setLevel(super.getLevel()+1);
    }
}
