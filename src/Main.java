import classes.CharacterClasses.Mage;
import classes.CharacterClasses.Warrior;
import classes.HelperClasses.ArmorType;
import classes.HelperClasses.EquipmentSlot;
import classes.HelperClasses.PrimaryAttributes;
import classes.Items.Armor;
import classes.Items.Weapon;
import classes.HelperClasses.WeaponType;

public class Main {

    public static void main(String[] args){

        //for testing
        Mage mage = new Mage("Raistlin Majere");
        mage.levelUp();

        Warrior warrior = new Warrior("King Arthur");

        Weapon staffOfMagius = new Weapon ("Staff of Magius", 2, WeaponType.STAFF, 4, 1);
        Weapon excalibur = new Weapon ("Excalibur", 1, WeaponType.SWORD, 8, 2);

        Armor mageRobe = new Armor("Standard Mage Robe", 1, ArmorType.CLOTH, new PrimaryAttributes(0,1,3)) ;
        Armor heavyPlate = new Armor("Standard Heavy Plate", 1, ArmorType.PLATE, new PrimaryAttributes(3,6,0)) ;


        try{
            mage.equipItem(staffOfMagius, EquipmentSlot.WEAPON);
            mage.equipItem(mageRobe, EquipmentSlot.BODY);
        }catch(Exception e){
            System.out.println(e);
        }

        System.out.println(mage.showCharacterStats());

    }
}
