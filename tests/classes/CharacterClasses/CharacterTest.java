package classes.CharacterClasses;
import classes.HelperClasses.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    public void character_islevel1_whenCreated(){
        Warrior warrior = new Warrior("King Arthur");
        int expected=1;
        int actual = warrior.getLevel();

        assertEquals(1, warrior.getLevel());
    }

    @Test
    public void levelUp_level_shouldbe2(){
        Warrior warrior = new Warrior("King Arthur");
        warrior.levelUp();
        int expected=2;
        int actual = warrior.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void warrior_gainsCorrectStats_whenCreated() {
        Warrior warrior = new Warrior("King Arthur");

        PrimaryAttributes baseAttributesWarrior = warrior.getBaseAttributes();

        int expected=5;
        int actual = baseAttributesWarrior.getStr();

        assertEquals(expected, actual);

        expected=2;
        actual = baseAttributesWarrior.getDex();

        assertEquals(expected, actual);

        expected=1;
        actual = baseAttributesWarrior.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void mage_gainsCorrectStats_whenCreated() {
        Mage mage = new Mage("mage");

        PrimaryAttributes baseAttributes = mage.getBaseAttributes();

        int expected=1;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=1;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=8;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void ranger_gainsCorrectStats_whenCreated() {
        Ranger ranger = new Ranger("ranger");

        PrimaryAttributes baseAttributes = ranger.getBaseAttributes();

        int expected=1;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=7;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=1;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void roque_gainsCorrectStats_whenCreated() {
        Roque roque = new Roque("roque");

        PrimaryAttributes baseAttributes = roque.getBaseAttributes();

        int expected=2;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=6;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=1;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void levelUp_warrior_statsShouldIncreaseCorrectly() {
        Warrior warrior = new Warrior("King Arthur");
        warrior.levelUp();

        PrimaryAttributes baseAttributesWarrior = warrior.getBaseAttributes();

        int expected=8;
        int actual = baseAttributesWarrior.getStr();

        assertEquals(expected, actual);

        expected=4;
        actual = baseAttributesWarrior.getDex();

        assertEquals(expected, actual);

        expected=2;
        actual = baseAttributesWarrior.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void levelUp_mage_statsShouldIncreaseCorrectly() {
        Mage mage = new Mage("mage");
        mage.levelUp();

        PrimaryAttributes baseAttributes = mage.getBaseAttributes();

        int expected=2;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=2;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=13;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void levelUp_ranger_statsShouldIncreaseCorrectly() {
        Ranger ranger = new Ranger("ranger");
        ranger.levelUp();

        PrimaryAttributes baseAttributes = ranger.getBaseAttributes();

        int expected=2;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=12;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=2;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }

    @Test
    public void levelUp_roque_statsShouldIncreaseCorrectly() {
        Roque roque = new Roque("roque");
        roque.levelUp();

        PrimaryAttributes baseAttributes = roque.getBaseAttributes();

        int expected=3;
        int actual = baseAttributes.getStr();

        assertEquals(expected, actual);

        expected=10;
        actual = baseAttributes.getDex();

        assertEquals(expected, actual);

        expected=2;
        actual = baseAttributes.getIntel();

        assertEquals(expected, actual);

    }


}