package classes.CharacterClasses;

import classes.Exceptions.InvalidArmorException;
import classes.Exceptions.InvalidWeaponException;
import classes.HelperClasses.ArmorType;
import classes.HelperClasses.EquipmentSlot;
import classes.HelperClasses.PrimaryAttributes;
import classes.HelperClasses.WeaponType;
import classes.Items.Armor;
import classes.Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EquipmentTest {

    @Test
    public void equip_WeaponofTooHighLevel_shouldThrowInvalidWeaponException(){
        Warrior warrior = new Warrior("King Arthur");
        Weapon excalibur = new Weapon ("Excalibur", 2, WeaponType.SWORD, 8, 2);

        String expected = "The weapon you are trying to equip is not allowed for this character type" +
                                       "or the character does not have the required level to equip this item.";

        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> warrior.equipItem(excalibur, EquipmentSlot.WEAPON));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equip_ArmorofTooHighLevel_shouldThrowInvalidArmorException(){
        Warrior warrior = new Warrior("King Arthur");
        Armor heavyPlate = new Armor("Standard Heavy Plate", 2, ArmorType.PLATE, new PrimaryAttributes(3,6,0)) ;

        String expected = "The weapon you are trying to equip is not allowed for this character type" +
                "or the character does not have the required level to equip this item.";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> warrior.equipItem(heavyPlate, EquipmentSlot.BODY));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equip_WeaponofWrongType_shouldThrowInvalidWeaponException(){
        Warrior warrior = new Warrior("King Arthur");
        Weapon staffOfMagius = new Weapon ("Staff of Magius", 1, WeaponType.STAFF, 4, 1);

        String expected = "The weapon you are trying to equip is not allowed for this character type" +
                "or the character does not have the required level to equip this item.";

        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> warrior.equipItem(staffOfMagius, EquipmentSlot.WEAPON));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equip_ArmorofWrongType_shouldThrowInvalidArmorException(){
        Warrior warrior = new Warrior("King Arthur");
        Armor mageRobe = new Armor("Standard Mage Robe", 1, ArmorType.CLOTH, new PrimaryAttributes(0,1,3)) ;

        String expected = "The weapon you are trying to equip is not allowed for this character type" +
                "or the character does not have the required level to equip this item.";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> warrior.equipItem(mageRobe, EquipmentSlot.BODY));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void equip_validWeapon_shouldReturnTrue(){
        Warrior warrior = new Warrior("King Arthur");
        Weapon sword = new Weapon ("sword", 1, WeaponType.SWORD, 8, 2);

        try{
            warrior.equipItem(sword, EquipmentSlot.WEAPON);
        }catch(Exception e){
            System.out.println(e);
        }

        assertTrue(warrior.getSpecificEquipment(EquipmentSlot.WEAPON) != null);

    }

    @Test
    public void equip_validArmor_shouldReturnTrue(){
        Warrior warrior = new Warrior("King Arthur");
        Armor heavyPlate = new Armor("Standard Heavy Plate", 1, ArmorType.PLATE, new PrimaryAttributes(3,6,0)) ;

        try{
            warrior.equipItem(heavyPlate, EquipmentSlot.BODY);
        }catch(Exception e){
            System.out.println(e);
        }

        assertTrue(warrior.getSpecificEquipment(EquipmentSlot.BODY) != null);

    }

    @Test
    public void calculateDamage_noWeapon_shouldReturnCorrectDPS(){
        Warrior warrior = new Warrior("King Arthur");

        float expected = (float) (1.0*(1.0+(5.0/100.0)));
        warrior.calculateDamage();
        float actual = warrior.getDamage();

        assertEquals(expected, actual);
    }

    @Test
    public void calculateDamage_WeaponEquiped_shouldReturnCorrectDPS(){
        Warrior warrior = new Warrior("King Arthur");
        Weapon excalibur = new Weapon ("Excalibur", 1, WeaponType.SWORD, 8, 2);

        try{
            warrior.equipItem(excalibur, EquipmentSlot.WEAPON);
        }catch(Exception e){
            System.out.println(e);
        }

        float expected = (float) ((8.0*2.0)*(1.0+(5.0/100.0)));
        float actual = warrior.getDamage();

        assertEquals(expected, actual);
    }

    @Test
    public void calculateDamage_WeaponAndArmorEquiped_shouldReturnCorrectDPS(){
        Warrior warrior = new Warrior("King Arthur");
        Weapon excalibur = new Weapon ("Excalibur", 1, WeaponType.SWORD, 8, 2);
        Armor heavyPlate = new Armor("Standard Heavy Plate", 1, ArmorType.PLATE, new PrimaryAttributes(3,6,0)) ;

        try{
            warrior.equipItem(excalibur, EquipmentSlot.WEAPON);
            warrior.equipItem(heavyPlate, EquipmentSlot.BODY);
        }catch(Exception e){
            System.out.println(e);
        }
        float expected = (float) ((8.0*2.0)*(1.0+(8.0/100.0)));
        float actual = warrior.getDamage();

        assertEquals(expected, actual);
    }




}